<!DOCTYPE html>
<html>
<head>
	
	<title>Animated Login Form</title>
	<link  rel="icon"   href="favicon.png" type="image/png" />
	<link rel="stylesheet" type="text/css" href="assets/css/styleLogin.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
</head>
<body>
	<img class="wave" src="assets/img/w.png">
	<div class="container">
		<div class="img">
			<img src="assets/img/bg.svg">
		</div>
		<div class="login-content">
			<form >
				<img src="assets/img/avatar.svg">
				<h2 class="title">Bienvenido</h2>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>Usuario</h5>
           		   		<input id="username" type="text" class="input"  >
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>Contraseña</h5>
           		    	<input id="password" type="password" class="input" >
            	   </div>
            	</div>
            	<input id="btninicio" type="reset" class="btn" value="Iniciar Sesión">
            </form>
        </div>
    </div>
	  <script src=""></script>
	  
	<!-- jQuery 3 -->
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<!-- jQuery Login -->
	<script type="text/javascript" src="assets/js/mainLogin.js"></script>
	<!-- jQuery Login -->
	<script src="view/js/seguridad/login.js"></script>
	
</body>
</html>