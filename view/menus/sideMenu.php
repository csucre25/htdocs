<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="../panel/dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li><a href="../cliente/cliente.php"><i class="fa fa-user"></i> <span>Cliente</span></a></li>

        <li><a href="../documento/documento.php"><i class="fa fa-file-text"></i> <span>Documento</span></a></li>

        <li><a href="../gestion/letra.php"><i class="fa  fa-font"></i> <span>Gestión Letra</span></a></li>


        <!--
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../ventas/cliente.php"><i class="fa fa-circle-o"></i> Clientes</a></li>
            <li><a href="../ventas/ventaCliente.php"><i class="fa fa-circle-o"></i> Realizar Ventas</a></li>
          </ul>
        </li>-->
        <li class="header">REPORTES</li>
        <!--<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Flujo de Caja</span></a></li>
        
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Pedidos</span></a></li>-->

        <li><a href="../reporte/rletra.php"><i class="fa fa-circle-o text-yellow"></i> <span>Consultar Letra</span></a></li>

        <li class="header">SEGURIDAD</li>

        <li><a href="../seguridad/usuario.php"><i class="fa fa-circle-o text-blue"></i> <span>Usuarios</span></a></li>

        </ul>
    