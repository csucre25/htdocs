<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA | Ventas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
   <link rel="stylesheet" href="../../assets/css/select.dataTables.min.css">
   <!-- Main style -->
  <link rel="stylesheet" href="../../assets/css/min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../assets/css/select2.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- top menu -->
     <?php 
      require('../menus/topNavigation.php');
     ?>
    <!-- /top menu -->
  </header>

  <!-- Columna vertical -->
  <aside class="main-sidebar">
    <section class="sidebar">

      <!-- top menu -->
      <?php 
        require('../menus/topMenu.php');
      ?>
      <!-- /top menu -->
      
      <!-- sidebar menu -->
      <?php 
        require('../menus/sideMenu.php');
      ?>
      <!-- /sidebar menu -->
    </section>
  </aside>

  <!-- ***** Contenido de la página *****-->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        GESTIÓN LETRA
        <small>Página</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Gestión Letra</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<!-- Inicio vista con pestaña-->

<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
              <li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>
              <!--li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>-->
            </ul>
           
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <DIV CLASS="CONTAINER">
              <DIV CLASS="ROW">
                   <div class="col-sm-12">

                   <form class="form-horizontal"> 

                    <div class="form-group">
                    <label class="col-md-1 col-sm-2 control-label">CLIENTE</label>
                    <div class="col-sm-4">
                       <select id="cboCliente" class="form-control select2" style="width:100%;">
                       </select>
                    </div>

                    <label for="txtFechaInicio" class="col-sm-1 control-label">Del:</label>
                    <div class="col-sm-2">
                        <input type="date" class="form-control" id="txtFechaInicio" value="2020-01-01">
                    </div>

                    <label for="txtFechaFin" class="col-sm-1 control-label">Al:</label>
                    <div class="col-sm-2">
                        <input type="date" class="form-control" id="txtFechaFin" value="2020-12-31" >
                    </div>

                    </div>

                  </form>

                   <form class="form-horizontal"> 

                    <div class="form-group">

                    <label class="col-md-1 col-sm-2 control-label">Estado</label>
                    <div class="col-sm-2">
                       <select id="cboEstado" class="form-control select2" style="width:100%;">
                       </select>
                    </div>
                    
                    <div class="col-sm-1">
                    <button type="button"  class="btn btn-info btn-md " id="btnBuscar">Buscar</button>
                    </div>

                    </div>

                    </form>

                    </div>
              </DIV>
              <DIV CLASS="ROW">
                  <DIV CLASS="COL-SM-12">
              <table id="datatable-Letra" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nº Letra</th>
                  <th>Plazo</th>
                  <th>Fecha Emisión</th>
                  <th>Fecha Vencimiento</th>
                  <th>Girador</th>
                  <th>Estado</th>
                  <th>Pago</th>
                  <th>Extorno</th>
                </tr>
                </thead>
                <tbody id="bodytable-Letra">
                </tbody>
              </table>
                </DIV>
              </DIV>
             </DIV>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
              <div class="box-body">
            <form class="form-horizontal">  
                <div class="row">
                    <div class="col-sm-7 panel panel-default">
                        <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-12">
                                        Documentos Comerciales
                                    </div>
                                </div>
</br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="datatable-Documento" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                            <th>Check</th>
                                            <th>Tipo Doc</th>
                                            <th>Serie</th>
                                            <th>Número</th>
                                            <th>Monto</th>
                                            <th>idDocumento</th>
                                            </tr>
                                            </thead>
                                            <tbody id="bodytable-Documento">
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="totalLetra" class="col-sm-3 control-label">Total Letra S/.</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="totalLetra" placeholder="0.00" disabled>
                                        </div>
                                    </div>
                                </div>
                            
                             
                        </div>
                    </div>
                   
                    <div class="col-sm-5 panel panel-default">
                        <div class="panel-body">

                       
                          <div class="row">
                              <div class="col-sm-12">
                                 Datos de la letra a canjear
                              </div>
                          </div>
                          </br>
                          <div class="row">
                              <div class="col-sm-12">
                                   <div class="form-group">

                            <label for="txtGirador" class="col-sm-3 control-label">Girador:</label>
                            <div class="col-sm-8">
                               <input type="text" class="form-control" id="txtGirador" ></input>
                            </div>

                        </div>

                         
                        <div class="form-group">

                            <label for="txtLugar" class="col-sm-3 control-label">Lugar:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="txtLugar" placeholder="lugar">
                            </div>

                        </div>

                        <div class="form-group">

                            <label for="txtFechaGiro" class="col-sm-3 control-label">Fecha Giro:</label>
                            <div class="col-sm-6">
                                <input type="date" class="form-control" id="txtFechaGiro" >
                            </div>

                        </div>

                         <div class="form-group">

                            <label for="txtPlazo" class="col-sm-3 control-label">Plazo:</label>
                            <div class="col-sm-4">
                                <input type="number" class="form-control" id="txtPlazo" placeholder="0">
                            </div>

                        </div>

                        <div class="form-group">

                            <label for="txtFechaVencimiento" class="col-sm-3 control-label">Fecha Vencimiento:</label>
                            <div class="col-sm-6">
                                <input type="date" class="form-control" id="txtFechaVencimiento" >
                            </div>

                        </div>

                        <div class="form-group">

                            <label for="txtLetra" class="col-sm-3 control-label">Letra:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txtLetra" placeholder="letra">
                            </div>

                        </div>
                              </div>
                          </div>
                             

                       

                        </div>
                    </div>
                </div>
                 
            </form>
            </div>
            <div class="box-footer">
                <div class="row ">
                <div class="col-md-9 col-md-offset-4 col-sm-9 col-sm-offset-4 col-xs-9 col-xs-offset-3">
                <button type="button"  class="btn btn-info btn-md" id="btnRegistrar">Registrar</button>
                <!--<button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>-->
                <button type="button"  class="btn btn-danger btn-md" id="btnCancelar">Cancelar</button>
                </div>
            </div>   
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
      </div>
      <!-- /.row -->






<!--Fin vista con pestaña-->

    </section>
    
    <!-- /.content -->
  </div>
  <!-- ***** Fin del contenido de la página *****-->

  <!-- footer content -->
  <?php 
    require('../menus/footerContent.php');
  ?>
  <!-- /footer content -->

</div>
<!-- ./wrapper -->
<!-- modal  pago-->
<div class="modal fade" id="mdlPago">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="tltPago">Registrar Pago</h4>
        </div>
        <div class="modal-body">
          <div class="row">            
            <form id="demo-formPago" class="form-horizontal form-label-left">
                <div class="form-group">
                    <label for="txtLetra" class="col-sm-3 control-label">Letra #</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="txtLetraPago" disabled placeholder="Nro de Letra">
                    </div>
                </div>
                <div class="form-group">

                    <label for="txtFechaPago" class="col-sm-3 control-label">Fecha Pago:</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" id="txtFechaPago" placeholder="dd/mm/aaaa">
                    </div>
                </div>
                <div class="form-group"> 
                    <label for="txtObservaciones" class="col-sm-3 control-label">Observaciones</label>
                    <div class="col-sm-6">
                    <input type="text" class="form-control" id="txtObservacionesPago" placeholder="Observaciones">
                    </div>
                </div>  
                <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="button" class="btn btn-primary" id="btnRegistrarPago" data-dismiss="modal" >Guardar</button>
                  <button type="button" class="btn btn-danger" id="btnCancelarPago" data-dismiss="modal">Cancelar</button>
                </div>
              </div>                
            </form>
            
          </div>
        </div>
      </div>
    </div>    
  </div>


<!-- modal  extorno-->
<div class="modal fade" id="mdlExtorno">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Registrar Extorno</h4>
        </div>
        <div class="modal-body">
          <div class="row">            
            <form id="demo-formExtorno" class="form-horizontal form-label-left">
                <div class="form-group">
                    <label for="txtLetra" class="col-sm-3 control-label">Letra #</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="txtLetraExtorno" disabled placeholder="Nro de Letra">
                    </div>
                </div>
                <div class="form-group">

                    <label for="txtFechaExtorno" class="col-sm-3 control-label">Fecha Extorno:</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control" id="txtFechaExtorno" placeholder="dd/mm/aaaa">
                    </div>
                </div>
                <div class="form-group"> 
                    <label for="txtObservaciones" class="col-sm-3 control-label">Observaciones</label>
                    <div class="col-sm-6">
                    <input type="text" class="form-control" id="txtObservacionesExtorno" placeholder="Observaciones">
                    </div>
                </div>  
                <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <button type="button" class="btn btn-primary" id="btnRegistrarExtorno" data-dismiss="modal" >Guardar</button>
                  <button type="button" class="btn btn-danger" id="btnCancelarExtorno" data-dismiss="modal">Cancelar</button>
                </div>
              </div>                
            </form>
            
          </div>
        </div>
      </div>
    </div>    
  </div>


<!-- jQuery 3 -->
<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/js/demo.js"></script>
<!-- DataTables -->
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../assets/js/select2.full.min.js"></script>
<!-- Capa JS -->
<script src="../js/gestion/letra.js"></script>


</body>
</html>
