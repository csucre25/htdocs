<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA | Ventas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
   <!-- DataTables -->
   <link rel="stylesheet" href="../../assets/css/dataTables.bootstrap.min.css">
   <!-- Main style -->
  <link rel="stylesheet" href="../../assets/css/min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../../assets/css/select2.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- top menu -->
     <?php 
      require('../menus/topNavigation.php');
     ?>
    <!-- /top menu -->
  </header>

  <!-- Columna vertical -->
  <aside class="main-sidebar">
    <section class="sidebar">

      <!-- top menu -->
      <?php 
        require('../menus/topMenu.php');
      ?>
      <!-- /top menu -->
      
      <!-- sidebar menu -->
      <?php 
        require('../menus/sideMenu.php');
      ?>
      <!-- /sidebar menu -->
    </section>
  </aside>

  <!-- ***** Contenido de la página *****-->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DOCUMENTO
        <small>Página</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Documento</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<!-- Inicio vista con pestaña-->

<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Consultar</a></li>
              <li><a href="#tab_2" data-toggle="tab">Nuevo</a></li>
              <!--li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>-->
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <table id="datatable-Documento" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Razón Social</th>
                  <th>Tipo Doc.</th>
                  <th>Estado</th>
                  
                  <th>Fecha Emisión</th>
                  <th>Serie</th>
                  <th>Número</th>
                  <th>Opciones</th>
                </tr>
                </thead>
                <tbody id="bodytable-Documento">
                
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
              <div class="box-body">
            <form class="form-horizontal">  
                <div class="form-group">
                    <label class="col-sm-2 control-label">RAZÓN SOCIAL</label>
                    <div class="col-sm-5">
                       <select id="cboRazon" class="form-control select2" style="width:100%;">
                       </select>
                    </div>
                </div>
</br>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tipo Documento</label>
                    <div class="col-sm-3">
                       <select id="cboTipoDoc" class="form-control select2" style="width:100%;">
                       </select>
                    </div>

                    <label for="fechaEmision" class="col-sm-2 control-label">Fecha emisión:</label>
                    <div class="col-sm-3">
                        <input type="date" class="form-control" id="txtFechaEmision" placeholder="dd/mm/aaaa">
                    </div>
                </div>  

                <div class="form-group">

                    <label for="serie" class="col-sm-2 control-label">Serie:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txtSerie" placeholder="serie">
                    </div>
                    <label for="numero" class="col-sm-2 control-label">Número:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="txtNumero" placeholder="número">
                    </div>

                </div>
                <div class="form-group">

                    <label for="valorVenta" class="col-sm-2 control-label">Valor Venta:</label>
                    <div class="col-sm-3">
                        <input  type="number" step="0.01" class="form-control" id="txtValorVenta" placeholder="0.00">
                    </div>
                    <label for="igv" class="col-sm-2 control-label">IGV (%):</label>
                    <div class="col-sm-3">
                        <input  type="number" step="0.01" class="form-control" id="txtIgv" placeholder="IGV">
                    </div>

                </div>
                <div class="form-group">

                    <label for="serie" class="col-sm-2 control-label">Total:</label>
                    <div class="col-sm-3">
                        <input  type="number" step="0.01" class="form-control" id="txtTotal" placeholder="0.00" disabled>
                    </div>
                    

                </div>
                 
            </form>
            </div>
            <div class="box-footer">
                <button type="button"  class="btn btn-info btn-md" id="btnRegistrar">Registrar</button>
                <button type="button"  class="btn btn-success btn-md" id="btnEditar">Guardar cambios</button>
                <button type="button"  class="btn btn-danger btn-md" id="btnCancelar">Cancelar</button>
            </div>   
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
      </div>
      <!-- /.row -->






<!--Fin vista con pestaña-->

    </section>
    
    <!-- /.content -->
  </div>
  <!-- ***** Fin del contenido de la página *****-->

  <!-- footer content -->
  <?php 
    require('../menus/footerContent.php');
  ?>
  <!-- /footer content -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/js/demo.js"></script>
<!-- DataTables -->
<script src="../../assets/js/jquery.dataTables.min.js"></script>
<script src="../../assets/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../assets/js/select2.full.min.js"></script>
<!-- BlockUI -->
<script src="../../assets/js/blockui.js"></script>
<!-- Capa JS -->
<script src="../js/documento/documento.js"></script>


</body>
</html>