<?php 
    session_start();  
    if(!isset($_SESSION['S_Cargo']) )
    {
      if($_SESSION['S_Cargo']!='Admin'){
        header("Location:../../index.php");   
      }
    }
    else
    {
      date_default_timezone_set('America/Lima');
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEMA | Ventas</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins -->
  <link rel="stylesheet" href="../../assets/css/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- top menu -->
     <?php 
      require('../menus/topNavigation.php');
     ?>
    <!-- /top menu -->
  </header>

  <!-- Columna vertical -->
  <aside class="main-sidebar">
    <section class="sidebar">

      <!-- top menu -->
      <?php 
        require('../menus/topMenu.php');
      ?>
      <!-- /top menu -->
      
      <!-- sidebar menu -->
      <?php 
        require('../menus/sideMenu.php');
      ?>
      <!-- /sidebar menu -->
    </section>
  </aside>

  <!-- ***** Contenido de la página *****-->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    
      <h1>
        Dashboard
        <small>Gráficos de estados de Letras y Documentos.</small>
      </h1>
      <ol class="breadcrumb">
        
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
</br>
<div class="row">
<div class="col-md-12">
      <form class="form-horizontal"> 

                    <div class="form-group">
                    
                    <label for="txtFechaInicio" class="col-sm-1 control-label">Del:</label>
                    <div class="col-sm-4 col-md-3 col-lg-2">
                        <input type="date" class="form-control" id="txtFechaInicio" onchange="handler(event);">
                    </div>

                    <label for="txtFechaFin" class="col-sm-1 control-label">Al:</label>
                    <div class="col-sm-4 col-md-3 col-lg-2">
                        <input type="date" class="form-control" id="txtFechaFin"  onchange="handler(event);" >
                    </div>

                    </div>

     </form>
     </div>
</div>
    </section>
    <section class="content">

    <div class="row">
            <div class=" col-md-4">

            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Documentos (Estado)</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Pendiente</li>
                    <li><i class="fa fa-circle-o text-aqua"></i> Canjeado</li>
                    <li><i class="fa fa-circle-o text-orange"></i> Anulado</li>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#">PENDIENTES
                  <span class="pull-right text-red"><i class="fa fa-file"></i> 2</span></a></li>
                <li><a href="#">CANJEADOS 
                <span class="pull-right text-aqua"><i class="fa fa-file"></i> 5</span></a></li>
                <li><a href="#">ANULADOS
                  <span class="pull-right text-orange"><i class="fa fa-file"></i> 0</span></a></li>
              </ul>
            </div>
            <!-- /.footer -->
          </div>

            </div>

    </section>

   
    <!-- /.content -->
  </div>
  <!-- ***** Fin del contenido de la página *****-->

  <!-- footer content -->
  <?php 
    require('../menus/footerContent.php');
  ?>
  <!-- /footer content -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../assets/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../assets/js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../assets/js/adminlte.min.js"></script>
<!-- ChartJS -->
<script src="../../assets/js/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../js/panel/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../assets/js/demo.js"></script>

</body>
</html>
