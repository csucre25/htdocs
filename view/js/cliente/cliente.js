//var a espanish datatable
$("#datatable-Cliente").DataTable({
    //dom: 'lfrtip',
    language: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "lengthMenu": [10, 25, 50, 75, 100]
});


//VARIABLES 
var cboMarca = document.getElementById('cboMarca');
var txtDescripcion = document.getElementById('txtDescripcion');
var txtStock = document.getElementById('txtStock');
var txtUnidadMedida = document.getElementById('txtUnidadMedida');
var txtPrecioVenta = document.getElementById('txtPrecioVenta');
var txtPrecioCosto = document.getElementById('txtPrecioCosto');
var btnGuardar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnEditar = document.getElementById('btnEditar');

var txtRazonSocial = document.getElementById('txtRazonSocial');
var txtRUC = document.getElementById('txtRUC');
var cboZona = document.getElementById('cboZona');
var txtResponsable = document.getElementById('txtResponsable');
var txtDNIResponsable = document.getElementById('txtDNIResponsable');
var idCliente;

//funciones



function obtenerID(id, RazonSocial, Responsable, RUC, DNIResponsable, idZona) {
    idCliente = id;
    txtRazonSocial.value = RazonSocial;
    cboZona.value = idZona;
    txtResponsable.value = Responsable;
    txtRUC.value = RUC;
    txtDNIResponsable.value = DNIResponsable;
    btnEditar.style.display = 'inline-block';
    btnCancelar.style.display = 'inline-block';
    btnGuardar.style.display = 'none';
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
}

function listarClientes() {

    var param_opcion = 'listCliente';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var table = $("#datatable-Cliente").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(e.RUC);
                array.push(e.RazonSocial);
                array.push(e.Responsable);
                array.push(e.DNIResponsable);
                array.push("<button href='#modalUpdate' data-toggle='modal' class='btn btn-xs btn-success' onclick='obtenerID(" + e.idCliente + "," + '"' + e.RazonSocial + '"' + "," + '"' + e.Responsable + '"' + "," + '"' + e.RUC + '"' + "," + '"' + e.DNIResponsable + '"' + "," + '"' + e.idZona + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminarCliente(" + e.idCliente + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });

        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });

}

function guardar() {

    var opcion = 'insertCliente';
    var RazonSocial = txtRazonSocial.value;
    var Responsable = txtResponsable.value;
    var RUC = txtRUC.value;
    var DNIResponsable = txtDNIResponsable.value;
    var idZona = cboZona.value;
    if (DNIResponsable.length != 8) {
        alert('DNI debe tener 8 digitos');
        return;
    }
    if (RUC.length != 11) {
        alert('RUC debe tener 11 digitos');
        return;
    }
    if (RazonSocial != "" & idZona != '0' & Responsable != '' & RUC != "" &
        DNIResponsable != '' & DNIResponsable.length == 8 && RUC.length == 11) {
        $("#btnRegistrar").attr("disabled");
        $.ajax({
            type: 'POST',
            data: 'opcion=' + opcion +
                '&RazonSocial=' + RazonSocial +
                '&Responsable=' + Responsable +
                '&RUC=' + RUC +
                '&DNIResponsable=' + DNIResponsable +
                '&idZona=' + idZona,
            url: '../../controlador/cliente/cCliente.php',
            success: function(data) {
                $("#btnRegistrar").removeAttr("disabled");
                var obj = JSON.parse(data);
                listarClientes();
                if (obj == 1) {
                    alert('Cliente registrada con exito.');
                    llenarcboZona();
                    idCliente = 0;
                    txtRazonSocial.value = "";
                    cboZona.value = 0;
                    txtResponsable.value = "";
                    txtRUC.value = "";
                    txtDNIResponsable.value = "";
                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                    //$('.select2').select2();
                } else if (obj == 0) {
                    alert('Cliente ya se encuentra regitrado.');
                }
            },
            error: function(data) {
                alert('Error al registrar');
                $("#btnRegistrar").removeAttr("disabled");
            }
        });
    } else {
        alert('Llenar campos');

    }

}

function editar() {

    var opcion = 'updateCliente';
    var RazonSocial = txtRazonSocial.value;
    var Responsable = txtResponsable.value;
    var RUC = txtRUC.value;
    var DNIResponsable = txtDNIResponsable.value;
    var idZona = cboZona.value;
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion +
            '&idCliente=' + idCliente +
            '&RazonSocial=' + RazonSocial +
            '&Responsable=' + Responsable +
            '&RUC=' + RUC +
            '&DNIResponsable=' + DNIResponsable +
            '&idZona=' + idZona,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            console.log(data);
            var obj = JSON.parse(data);
            listarClientes();
            if (obj == 1) {
                alert('Cliente editado con exito.');
                cancelar();

            } else if (obj == 4) {
                alert('RUC ya existe.');
            } else {
                alert('No se realizaron modificaciones.');
            }
        },
        error: function(data) {
            alert('Error al editar');
        }
    });
}

function eliminarCliente(idCliente) {

    var param_opcion = 'deleteCliente';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idCliente=' + idCliente,
            url: '../../controlador/cliente/cCliente.php',
            success: function(data) {
                var obj = JSON.parse(data);
                console.log(data);
                if (obj == 1) {

                    listarClientes();

                } else {
                    alert('Error al eliminar');
                }

            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }

}


function cancelar() {

    idCliente = 0;
    txtRazonSocial.value = '';
    cboZona.value = 0;
    txtResponsable.value = '';
    txtRUC.value = '';
    txtDNIResponsable.value = '';

    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnGuardar.style.display = 'inline-block';
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function llenarcboZona() {

    var param_opcion = 'listZona';
    $("#cboZona").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/zona/cZona.php',
        success: function(data) {
            var obj = JSON.parse(data);

            $("#cboZona").html('<option value="0">Seleccionar Zona...</option>');
            obj.forEach(function(e) {
                $("#cboZona").append('<option value="' + e.idZona + '">' + e.Zona + '</option>');
            });
        },
        error: function(data) {

        }
    });
}
function check(e,value)
{
    //Check Charater
    var unicode=e.charCode? e.charCode : e.keyCode;
    if (value.indexOf(".") != -1)if( unicode == 46 )return false;
    if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
}

function checkLength(len,ele){
    var fieldLength = ele.value.length;
    if(fieldLength <= len){
      return true;
    }
    else
    {
      var str = ele.value;
      str = str.substring(0, str.length - 1);
      ele.value = str;
    }
  }

function alCargarDocumento() {
    //$('#datatable-Producto').DataTable();
    listarClientes();
    llenarcboZona();
    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnCancelar.addEventListener("click", cancelar);
    btnGuardar.addEventListener("click", guardar);
    btnEditar.addEventListener("click", editar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);