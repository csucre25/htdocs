
//var a espanish datatable
var idioma_espanol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}

//VARIABLES 
var cboCargo= document.getElementById('cboCargo');
var Usuario= document.getElementById('Usuario');
var Contra= document.getElementById('Contra');
var ContraVerifica= document.getElementById('ContraVerifica');
var btnGuardar = document.getElementById('btnRegistrar');
var btnCancelar = document.getElementById('btnCancelar');
var btnEditar=document.getElementById('btnEditar');
var idUsuario;
//funciones
function obtenerID(idUsu,User,Cargo){
    idUsuario = idUsu;
    cboCargo.value=Cargo;
    Usuario.value=User;
    btnEditar.style.display = 'inline-block';
	btnCancelar.style.display = 'inline-block';
    btnGuardar.style.display='none';

    ContraVerifica.disabled=true;
    Contra.disabled=true;
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
}
function listarUsuario(){	
	var param_opcion = 'listarUsuario';
    $.ajax({
        type: 'POST',
        data: 'opcion=' + param_opcion,
        url: '../../controlador/seguridad/cUsuario.php',
        success: function(data) {
            console.log(data);
            var table = $("#datatable-Usuario").DataTable();
            table.clear().draw();
            var obj = JSON.parse(data);
            var cont = 0;
            console.log(obj);
            obj.forEach(function(e) {
                cont++;
                array = [];
                array.push(cont);
                array.push(e.Usuario);
                array.push(e.Cargo);
                array.push("<button class='btn btn-xs btn-success' onclick='obtenerID(" + e.idUsuario + "," + '"' + e.Usuario + '"' + "," + '"' + e.Cargo + '"' + ")'>Editar</button><button href='#modalEliminar' data-toggle='modal' class='btn btn-xs btn-danger' onclick='eliminar(" + e.idUsuario + ")'>Eliminar</button>");
                table.row.add(array).draw().node();
            });
        },
        error: function(data) {
            alert('Error al mostrar');
        }
    });
}
function guardar(){	
        var opcion = 'insert';
        var usuari=Usuario.value;
        var carg = cboCargo.value;
        var contr;
        if(Contra.value!=ContraVerifica.value){contr="0";}
        else if(ContraVerifica.value==Contra.value){contr=Contra.value;}
		if (carg!='0' & usuari!="" & contr!="0"){
            $.ajax({
			type: 'POST',
			data: 	'opcion=' + opcion +
                      '&Usuario=' + usuari+ '&Clave=' + contr+
					  '&Cargo=' + carg,
            url: '../../controlador/seguridad/cUsuario.php',
			success: function(data){
                var obj=JSON.parse(data);
				listarUsuario();
				if(obj == 1){	
					alert('Usuario registrado con exito.');	
	                idUsuario = 0;
                    cboCargo.value=0;
	                Usuario.value="";    
                    Contra.value="";
                    ContraVerifica.value="";
				}
				else if (obj == 0){
					alert('Error al registrar.');	
				}
			},
			error:function(data){
				alert('Error al registrar');
			}
		});}else{
			alert('Campos no válidos');
		}
}
function editar(){	
    var opcion = 'editar';
    var usuari=Usuario.value;
    var carg = cboCargo.value;
    if (usuari!="" & carg!='0'){
		$.ajax({
			type: 'POST',
			data: 	'opcion=' + opcion +
                    '&idUsuario=' + idUsuario + 
                    '&Usuario=' + usuari+
                    '&Cargo=' + carg,
            url: '../../controlador/seguridad/cUsuario.php',
			success: function(data){
				var obj=JSON.parse(data);
				listarUsuario();
				if (obj == 1) {
                    alert('Usuario editado con exito.');
                    cancelar();
    
                } else if (obj == 4) {
                    alert('Usuario ya existe.');
                }else{
                    alert('No se realizaron modificaciones.');
                }
			},
			error:function(data){
				alert('Error al editar');
			}
		});}else{
            alert('Campos no válidos');
        }
}
function eliminar(idusu){
	var param_opcion = 'delete';
    if (confirm("Seguro de Anular (S/N)??")) {
        $.ajax({
            type: 'POST',
            data: 'opcion=' + param_opcion +
                '&idUsuario=' + idusu,
            url: '../../controlador/seguridad/cUsuario.php',
            success: function(data) {
                var obj = JSON.parse(data);
                if (obj == 1) {
                    listarUsuario();
                } else {
                    alert('Error al eliminar');
                }
            },
            error: function(data) {
                alert('Error al eliminar');
            }
        });
    }
}
function cancelar() {
    idUsuario = 0;
    Usuario.value = '';
    cboCargo.value= 0;
    btnEditar.style.display = 'none';
    btnCancelar.style.display = 'none';
    btnGuardar.style.display = 'inline-block';
    ContraVerifica.disabled=false;
    Contra.disabled=false;

    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
}
function llenarcboCargo(){
    $("#cboCargo").empty();
    $("#cboCargo").html('<option value="0">Seleccionar Cargo...</option>');
    $("#cboCargo").append('<option value="' + 'Supervisor' + '">' + 'Supervisor' + '</option>');
    $("#cboCargo").append('<option value="' + 'Analista' + '">' + 'Analista' + '</option>');
    $("#cboCargo").append('<option value="' + 'Admin' + '">' + 'Admin' + '</option>');
}
function alCargarDocumento(){
   
	$('#datatable-Usuario').DataTable();
    listarUsuario(); 
    llenarcboCargo();
    btnEditar.style.display = 'none';
	btnCancelar.style.display = 'none';
	btnCancelar.addEventListener("click", cancelar);
	btnGuardar.addEventListener("click", guardar);
    btnEditar.addEventListener("click",editar);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);