var btnImprimir = document.getElementById('btnImprimir');

var fechaInicial=document.getElementById('fechaInicial');
var fechaFinal=document.getElementById('fechaFinal');

var cboRazon=document.getElementById('cboRazon');


function llenarcboClientes() {

    var opcion = 'listCliente';
    $("#cboRazon").empty();
    $.ajax({
        type: 'POST',
        data: 'opcion=' + opcion,
        url: '../../controlador/cliente/cCliente.php',
        success: function(data) {
            var obj = JSON.parse(data);
            console.log(obj);
            $("#cboRazon").html('<option value="0">Seleccionar Cliente...</option>');
            obj.forEach(function(e) {
                $("#cboRazon").append('<option value="' + e.idCliente + '">' + e.RazonSocial + '</option>');
            });
            $("#cboRazon").select2();
        },
        error: function(data) {

        }
    });
}

function imprimir(){
    var opcion = 'imprimir';
    var idCliente= cboRazon.value;
    var fechaInicio= fechaInicial.value;
    var fechaFin= fechaFinal.value;
    if(idCliente!='0'){
    $.ajax({
        type: 'POST',
        data: 'opcion='+opcion+
        '&idCliente=' + idCliente+
        '&fechaInicial=' + fechaInicio+
        '&fechaFinal=' + fechaFin,
        url: '../../controlador/reporte/crletra.php',
        success: function(data){
            var obj =JSON.parse(data);
            console.log(obj);
            if (obj.length>0){
                generaReporte(fechaInicio,fechaFin,obj);
            }else
            {alert('No hay datos para el reporte');}
            
           
        },
        error:function(data){
            alert('Error al mostrar');
        }
    });}else{
        alert("No selecciono una razon");
    }
}

function generaReporte(fechainicial,fechafinal,objeto){


    var doc = new jsPDF () ;
    doc.setFontSize(12);
    doc.text( 82, 30,'CALZADOS VENECIA') ;
    doc.setFontSize(11);
    doc.text( 50, 40,'LISTADO DE LETRAS DEL: ') ;
    doc.text(102,40,fechainicial)
    doc.text(125,40,'AL') ;
    doc.text(133,40,fechafinal)

    doc.setFontSize(9);
    doc.rect(20, 50, 20, 7);
    doc.text(22, 55,'TIPO DOC.');
    doc.rect(40, 50, 43, 7);
    doc.text(49, 55,'Nº DOC. / LETRA');
    doc.rect(83, 50, 26, 7);
    doc.text(89, 55,'IMPORTE');
    doc.rect(109, 50, 26, 7);
    doc.text(112, 55,'FECHA GIRO');
    doc.rect(135, 50, 25, 7);
    doc.text(137, 55,'PLAZO (DIAS)');
    doc.rect(160, 50, 30, 7);
    doc.text(164, 55,'FECHA VENCI.');
    var cont=0;
    var sum=0.00;
    var resultado = "A";
    objeto.forEach(function(e,index){   
        cont=cont+7;
        doc.setFontType('normal')
        doc.setFontSize(9);
        doc.rect(20, 50+cont, 20, 7);
        doc.text(22, 55+cont,e.TipoPago);
        doc.rect(40, 50+cont, 43, 7);
        doc.text(49, 55+cont,e.Numero);
        doc.rect(83, 50+cont, 26, 7);
        doc.text(89, 55+cont,e.Monto);
        doc.rect(109, 50+cont, 26, 7);
        /*doc.text(112, 55+cont,e.fechaEmision);*/
        doc.rect(135, 50+cont, 25, 7);
        /*doc.text(137, 55+cont,e.plazo);*/
        doc.rect(160, 50+cont, 30, 7);
        /*doc.text(164, 55+cont,e.fechaVencim);*/

        sum = sum + parseFloat(e.Monto);

        if(e.flagLetra == 'X'){
            resultado = sum.toString();
            cont=cont+7;
            doc.setFontSize(9);
         
            doc.setFontType('bold')
        doc.rect(20, 50+cont, 20, 7);
        doc.text(22, 55+cont,e.letra);
        doc.rect(40, 50+cont, 43, 7);
        doc.text(49, 55+cont,e.NroLetra);
        doc.rect(83, 50+cont, 26, 7);
        doc.text(89, 55+cont,resultado);
        doc.rect(109, 50+cont, 26, 7);
        doc.text(112, 55+cont,e.fechaEmision);
        doc.rect(135, 50+cont, 25, 7);
        doc.text(137, 55+cont,e.plazo);
        doc.rect(160, 50+cont, 30, 7);
        doc.text(164, 55+cont,e.fechaVencim);
        sum = 0;
        resultado = "B";
        }
	
	});

    $("#pdf_preview").attr("src", doc.output('datauristring')); 

  

   /* doc.save ( 'reporte-'+ hoyFecha()+idProducto+'.pdf');*/
			
}


function hoyFecha(){
    var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth()+1;
        var yyyy = hoy.getFullYear();
        
        dd = addZero(dd);
		mm = addZero(mm);
		var fecha;
		
			fecha=yyyy+'-'+mm+'-'+dd;
		
		
        return fecha;
}

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}



function alCargarDocumento(){
    llenarcboClientes();
    btnImprimir.addEventListener("click",imprimir);
}
//EVENTOS
window.addEventListener("load", alCargarDocumento);