<?php 

	class Conexion{
		
		protected $conexion_db;

		public function Conexion(){
			
			try{
				$this->conexion_db=new PDO('mysql:host=localhost;dbname=bdz001','root','');
				$this->conexion_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->conexion_db->exec("SET NAMES utf8"); 
				return $this->conexion_db;

			}catch(Exception $e){
				echo "Msg Error: ". $e->getLine();
			}
		}

	}

 ?>
