<?php 
    session_start();  
	include_once( "../../config/conexion.php");

	class Usuario_model extends Conexion{
		private $param = array();
	    private $con;
	    public function __construct(){
		    parent::__construct();
	    }	
	    public function gestionar($param){
	    	$this->param = $param;
	    	switch ($this->param['opcion'])
			{
                case 'inicioSesion':
                    echo $this->inicioSesion();
                    break;
				case 'listarUsuario':
					echo $this->listarUsuario();
					break;	
				case 'insert':
					echo $this->insert();
					break;	
				case 'editar':
					echo $this->editar();
					break;	
				case 'delete':
					echo $this->eliminar();
                    break;
                case 'llenarcboRol':
                    echo $this->llenarcboRol();
                    break;
			}
	    }	
		private function inicioSesion(){
			$usuario=$this->param['Usuario'];
			$clave=$this->param['Clave'];
			$sql="SELECT COUNT(*) from Usuario U WHERE U.Usuario='$usuario'
			and U.clave='$clave' and U.Activado=0";
			$sentencia=$this->conexion_db->query($sql);
			if ($sentencia->fetchColumn()==0) {
				$sql2="SELECT * from Usuario U WHERE U.Usuario='$usuario' 
				and U.clave='$clave' and U.Activado=1";
				$sentencia=$this->conexion_db->query($sql2);
				$sentencia->execute();
       			$resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
				$sentencia->closeCursor();
				$this->conexion_db=null;
				if (count($resultado) == 0) {
					return '4';
				}else{
					foreach ($resultado as $key => $v) {
						$_SESSION['S_IdUsuario']= utf8_encode($v["idUsuario"]);
						$_SESSION['S_Usuario']= utf8_encode($v["Usuario"]);
						$_SESSION['S_Cargo']= utf8_encode($v["Cargo"]);
					}
       				 return '1'; 
				}
			}else{
				return '0';
			}
		}
        private function listarUsuario(){
		$sql="SELECT U.idUsuario, U.Usuario, U.Clave, U.Cargo, U.Activado from usuario U where U.Activado = '1' order by U.Usuario ";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
		return json_encode($resultado);  
		}

		private function insert(){	    	
		$Usuario=$this->param["Usuario"];
        $Clave=$this->param["Clave"];
        $Cargo=$this->param["Cargo"];
        $sql="SELECT COUNT(*) from usuario u WHERE u.Usuario='$Usuario' and u.Activado <> 0";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $stmt;
            $sql="INSERT usuario(Usuario,Clave,Cargo) values(?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$Usuario,$Clave,$Cargo]);
            $this->conexion_db=null;
            return json_encode(1); 
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
		}
		private function editar(){	    	
			$idUsuario=$this->param["idUsuario"];
			$Usuario=$this->param["Usuario"];
			$Cargo=$this->param["Cargo"];
	
			$sql="SELECT COUNT(*) from Usuario WHERE Usuario='$Usuario' and idUsuario <> $idUsuario and Activado <> '0'";
			$sentencia=$this->conexion_db->query($sql);
			 if ($sentencia->fetchColumn()==0) {
				$sql="UPDATE usuario set Usuario='$Usuario',Cargo='$Cargo' where idUsuario=$idUsuario";
				$stmt= $this->conexion_db->prepare($sql);
				$stmt->execute();
				if ($stmt->rowCount()>0) {
					$this->conexion_db=null;
					return json_encode(1); 
				}else{
					$this->conexion_db=null;
					return json_encode(0); 
				}
			 }else{
				$this->conexion_db=null;
				return json_encode(4); 
			} 
	    }	 
	    private function eliminar(){
	    	$idUsuario=$this->param["idUsuario"];
       		$sql="UPDATE usuario set Activado = '0' where idUsuario=$idUsuario";
        	$stmt= $this->conexion_db->prepare($sql);
       		$stmt->execute();
       		if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        	}else{
            $this->conexion_db=null;
            return json_encode(0); 
       		}
        }
        private function llenarcboRol(){
            global $database;
			$sql = "exec sp_mnt_Usuario @peticion=1";
			$database->setQuery($sql);
			$rows=$database->loadObjectList();
            $combo = "<option value='0' disabled='disabled' selected='selected'>Seleccionar</option>";
		    foreach ($rows as $key => $v) {
                $rol=utf8_encode($v->rol);
                $combo .= "<option value='".$v->idRol."'>".$rol."</option>";
		    }
		    return $combo;
        }
        
        
		
	}
?>