<?php
session_start();

include_once( "../../config/conexion.php");

class Cliente_model extends Conexion{
    private $param = array();
    public $con;

    public function __constcodInternot(){
      parent::__constcodInternot();
    }

    public function crud($param){
        $this->param = $param;
        switch ($this->param['opcion'])
        {
            case 'listCliente':
                echo $this->listarCliente();
                break;
            case 'insertCliente':
            	echo $this->insertarCliente();
                break;
            case 'updateCliente':
                echo $this->actualizarCliente();
                break;
            case 'deleteCliente':
                echo $this->eliminarCliente();
                break;
        }
    }

    private function listarCliente(){
    	$sql="SELECT c.idCliente, c.RUC, c.RazonSocial, c.Responsable, c.DNIResponsable,c.idZona from cliente c order by idCliente";
        $sentencia=$this->conexion_db->prepare($sql);
        $sentencia->execute();
        $resultado=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        $sentencia->closeCursor();
        $this->conexion_db=null;
        return json_encode($resultado);  
    }

    private function insertarCliente(){
        $RazonSocial=$this->param["RazonSocial"];
        $Responsable=$this->param["Responsable"];
        $RUC=$this->param["RUC"];
        $DNIResponsable=$this->param["DNIResponsable"];
        $idZona=$this->param["idZona"];
        $sql="SELECT COUNT(*) from cliente c WHERE c.RUC='$RUC'";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $stmt;
            $sql="INSERT cliente(RazonSocial,Responsable,RUC,DNIResponsable,idZona) values(?,?,?,?,?)";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute([$RazonSocial,$Responsable,$RUC,$DNIResponsable,$idZona]);
            $this->conexion_db=null;
            return json_encode(1); 
        }    
        else{
            $this->conexion_db=null;
            return json_encode(0); 
        }      
    }    

    private function actualizarCliente(){

        $idCliente=$this->param["idCliente"];
        $RazonSocial=$this->param["RazonSocial"];
        $Responsable=$this->param["Responsable"];
        $RUC=$this->param["RUC"];
        $DNIResponsable=$this->param["DNIResponsable"];
        $idZona=$this->param["idZona"];

        $sql="SELECT COUNT(*) from cliente c WHERE c.RUC='$RUC' and c.idCliente <> $idCliente";
        $sentencia=$this->conexion_db->query($sql);
         if ($sentencia->fetchColumn()==0) {
            $sql="UPDATE cliente set RazonSocial='$RazonSocial',Responsable='$Responsable',RUC='$RUC',DNIResponsable='$DNIResponsable',idZona=$idZona where idCliente=$idCliente";
            $stmt= $this->conexion_db->prepare($sql);
            $stmt->execute();
            if ($stmt->rowCount()>0) {
                $this->conexion_db=null;
                return json_encode(1); 
            }else{
                $this->conexion_db=null;
                return json_encode(0); 
            }
         }else{
            $this->conexion_db=null;
            return json_encode(4); 
        } 

        

        
    }
    private function eliminarCliente(){
    	$idCliente=$this->param["idCliente"];
        $sql="DELETE FROM cliente WHERE idCliente=$idCliente";
        $stmt= $this->conexion_db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount()>0) {
            $this->conexion_db=null;
            return json_encode(1); 
        }else{
            $this->conexion_db=null;
            return json_encode(0); 
        }
    }
}
?>